// Import Modules

const express = require("express");

const mongoose = require("mongoose");

const cors = require("cors");

const dotenv = require("dotenv").config();

const bcrypt = require("bcrypt");

// [SECTION] Routes

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

// [SECTION] Connect Express

const app = express();
const port = 3001;

app.use(cors({origin: '*'}));
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// [Section] Connect Mongoose

mongoose.connect(`mongodb+srv://ronb077:${process.env.PASSWORD}@cluster0.umwequr.mongodb.net/eCommerce?retryWrites=true&w=majority`, {
    useNewUrlParser : true,
    useUnifiedTopology : true
});

let db = mongoose.connection
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to your Database"));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

app.listen(port, () => console.log(`Connection is now online on port ${port}`));