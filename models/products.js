const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  productName: {
    type: String,
    required: [true, "Product Name is required"],
  },

  description: {
    type: String,
    required: [true, "Product description is required"],
  },

  price: {
    type: Number,
    required: [true, "Price is required"],
  },

  image: {
    type: String
  },

  isActive: {
    type: String,
    default: true,
  },

  createdon: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Product", productSchema);
