const User = require("../models/users");
const Order = require("../models/orders");
const Product = require("../models/products");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// [All Users] Create Order

// ? exports a single function "createOrder" as a property of the module. It takes 3 parameters: data, userId, and newOrder.

module.exports.createOrder = async (data, userId, newOrder) => {
  // ? Checks if the data.isAdmin property is truthy, and returns a string "An Admin cannot place an order!"
  if (data.isAdmin) {
    return "An Admin cannot place an order!";
  }

  // ? Checks if the newOrder.products property is either undefined or has a length of 0, and returns a string "The order does not contain any product"
  if (newOrder.products === undefined || newOrder.products.length === 0) {
    return "The order does not contain any product";
  }

  // ? Initializes totalAmount to 0, then calculates the total amount of the order by iterating over the products in newOrder.products, finding the price of each product with the help of the Product.findById method, and adding the product's price multiplied by its quantity to totalAmount.
  let totalAmount = 0;
  for (let i = 0; i < newOrder.products.length; i++) {
    const product = newOrder.products[i];
    const { price } = await Product.findById(product.productId).catch(() => ({
      price: 0,
    }));
    totalAmount += price * product.quantity;
  }

  // ? Creates a new instance of the Order class with userId and totalAmount as properties.
  const new_order = new Order({
    userId,
    totalAmount,
    products: [...newOrder.products],
  });
  console.log(new_order)

  // ? Saves the newly created order instance to the database with the save method.
  await new_order.save();

  return {
    message: "Your order has been successfully placed!",
    new_order
  };
};
