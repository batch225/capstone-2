const Product = require("../models/products");

// [Admin Only] Create a product

module.exports.addProduct = (data) => {
  if (data.isAdmin) {
    let new_product = new Product({
      productName: data.product.productName,
      description: data.product.description,
      price: data.product.price,
      image: data.product.image
    });

    return new_product.save().then((new_product, error) => {
      if (error) {
        return false;
      }

      return {
        message: "You have successfully created a new product!",
        new_product
      };
    });
  }

  let message = Promise.resolve("User must be an Admin to add a new product!");

  return message.then((value) => {
    return value;
  });
};

// [Admin Only] Retrieve Active and Inactive products

module.exports.getAllProducts = () => {
    return Product.find({}).then((result) => {
        return result
    });
};

// [All Users] Retrive all active products

module.exports.getProducts = () => {
    return Product.find({isActive: true}).then((result) => {
        return result
    });
};

// [All Users] Retrieve a single product

module.exports.getSingleProduct = (productId) => {
    return Product.findById(productId).then(result => {
        return result
    });
};

// [Admin Only] Update product information

// module.exports.updateProduct = (reqParams, data) => {
//     if (data.isAdmin) {
//         let updatedProduct = {
//             productName: data.product.productName,
//             description: data.product.description,
//             price: data.product.price,
//             image: data.product.image
//         }; 

//         return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
//             if (error) {
//                 return false
//             } else {
//                 return {
//                     message: "Product successfully updated",
//                     product
//                 }
//             }
//         });
//     } 

//     let message = Promise.resolve("You must be an Admin to update a product!");

//     return message.then((value) => {
//       return value;
//     });
// };

module.exports.updateProduct = async (reqParams, data) => {
    try {
        if (data.isAdmin) {
            const updatedProduct = {
                productName: data.product.productName,
                description: data.product.description,
                price: data.product.price,
                image: data.product.image
            };
            const product = await Product.findByIdAndUpdate(reqParams.productId, updatedProduct);
            if (product) {
                return {
                    message: "Product successfully updated",
                    product
                };
            } else {
                return {
                    message: "Product not found"
                };
            }
        } else {
            return {
                message: "You must be an Admin to update a product!"
            };
        }
    } catch (error) {
        console.error(error);
        return {
            message: "Something went wrong!",
            error
        };
    }
};

// [Admin Only] Archive a product

module.exports.archiveProduct = (reqParams, data) => {
    if (data.isAdmin) {
        let updateActiveStatus = {
            isActive: false
        };

        return Product.findByIdAndUpdate(reqParams.productId, updateActiveStatus).then((product, error) => {
            if (error) {
                return false
            } else {
                return {
                    message: "You have successfully archived the following product:",
                    product
                }
            };
        });
    }

    let message = Promise.resolve("You must be an Admin to archive a product!");

    return message.then((value) => {
      return value;
    });
};

// [Admin Only] Activate a product

module.exports.activateProduct = (reqParams, data) => {
    if (data.isAdmin) {
        let Activate = {
            isActive: true
        };

        return Product.findByIdAndUpdate(reqParams.productId, Activate).then((product, error) => {
            if (error) {
                return false
            } else {
                return {
                    message: "You have successfully activated the following product:",
                    product
                }
            };
        });
    }

    let message = Promise.resolve("You must be an Admin to activate a product!");

    return message.then((value) => {
      return value;
    });
};


