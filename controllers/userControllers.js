const User = require("../models/users");
const Order = require("../models/orders");
const Product = require("../models/products");
const mongoose = require('mongoose');
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check Email

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// the find method returns a record if a match is found

		if (result.length > 0) {

			return true
		}

		return false
	})
}

// [All Users] User Registration Controller

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
	mobileNumber: reqBody.mobileNumber,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10)
  });

  return newUser.save().then((user, error) => {
    if (error) {
      return false;
    } else {
      return user;
    }
  });
};

// [Admin Only] Admin Registration Controller

module.exports.registerAdmin = (reqBody) => {
  let newAdmin = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
    isAdmin: true,
  });

  return newAdmin.save().then((user, error) => {
    if (error) {
      return false;
    } else {
      return user;
    }
  });
};

//  [All Users] User Authentication

module.exports.loginUser = (data) => {
	return User.findOne({email : data.email}).then(result => {
		if(result == null ){
			return {
				message: "Not found in our database"
			}
		} else {
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password);
			if (isPasswordCorrect) {
				return {access : auth.createAccessToken(result)}
			} else {
				return {
					message: "password was incorrect"
				}
			};
		};
	});
};

// Retrieve User

// module.exports.getUser = async (userId) => {
//   const user = await User.findById(userId);
//   const orders = await Order.find({ userId });
//   return { user, orders };
// };

// module.exports.getProfile = (userData) => {
// 	return User.findOne({id : userData.id}).then(result => {
// 		if (result == null){
// 			return false
// 		} else {
// 			result.password = ""
// 			return result
// 		}
// 	});
// };

module.exports.getProfile = (userData) => {
	return User.findOne({ email: userData.email })
	  .then(result => {
		console.log(result)
		if (result == null) {
		  return false;
		} else {
		  result.password = '';
		  return result;
		}
	  });
  };


