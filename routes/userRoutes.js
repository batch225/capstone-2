const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userControllers");

// Check Email
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(result => res.send(result));
});

// [Regular Users] User Registration Route

router.post("/register", (req,res) => {
    userController.registerUser(req.body).then((result) => res.send(result));
}); 

// [Admin Only] User Registration Route

router.post("/registerAdmin", (req, res) => {
    userController.registerAdmin(req.body).then((result) => res.send(result));
});


// [All Users] User Authentication

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then((result) => res.send(result));
});

// Retrieve User

// router.get("/:id", (req, res) => {
//     userController.getUser(req.params.id).then(result => res.send(result));
// });

// router.get("/details", auth.verify, (req, res) => {
    
//         const userData = auth.decode(req.headers.authorization)
//         console.log(userData)
//         console.log(req.headers.authorization)
    
//         userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController))
// });

router.get("/details", auth.verify, (req, res) => {
    const token = req.headers.authorization;
    const decoded = auth.decode(token);
    console.log("Token:", token);
    console.log("Decoded:", decoded);
  
    userController.getProfile({email: decoded.email}).then(resultFromController => res.send(resultFromController));
  });
  

module.exports = router;
