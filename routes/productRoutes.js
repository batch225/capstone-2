const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productController = require("../controllers/productControllers");

// [Admin Only] Create Product

router.post("/addProduct", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  productController.addProduct(data).then((result) => res.send(result));
});

// [Admin Only] Retrieve Active and Inactive products

router.get("/all", (req, res) => {
  productController.getAllProducts().then((result) => res.send(result));
});

// [All Users] Retrieve all active products

router.get("/", (req, res) => {
    productController.getProducts().then((result) => res.send(result));
});

// [All Users] Retrieve a single product

router.get("/:id", (req, res) => {
    productController.getSingleProduct(req.params.id).then(result => res.send(result));
});

// [Admin Only] Update product information

// router.put("/:productId", auth.verify, (req, res) => {
//     const data = {
//       product: req.body,
//       isAdmin: auth.decode(req.headers.authorization).isAdmin
//     };
//     productController.updateProduct(req.params, data).then((result) => res.send(result));
//   });

router.put("/:productId", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  productController
    .updateProduct(req.params, data)
    .then((result) => res.send(result))
    .catch((error) => res.status(500).send({ message: error.message }));
});


// [Admin Only] Archive a product

router.put("/archive/:productId", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };
    productController.archiveProduct(req.params, data).then((result) => res.send(result));
});

// [Admin Only] Activate a product

router.put("/activate/:productId", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };
    productController.activateProduct(req.params, data).then((result) => res.send(result));
});


module.exports = router;