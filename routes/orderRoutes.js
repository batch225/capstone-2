const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const userController = require("../controllers/userControllers");
const auth = require("../auth");

// [All Users] Create Order

router.post("/createOrder", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
}
  const userId = auth.decode(req.headers.authorization).id;

  orderController.createOrder(data, userId, req.body).then((result) => {
    res.send(result);
  });
});

module.exports = router;

